# Jury Final 16 janvier 2024 de 13h30 à 17h30

## Présentations PDF et abstract graphique

Déposez vos présentations pdf (16 slides) dans ce dossier et intulez-les de la manière suivante : "group-00.pdf" (en adaptant l'intitulé à votre numéro de groupe).

Déposez également votre abstract graphique (1 image PNG) et intulez-le de la manière suivante : "group-00.png" (en adaptant l'intitulé à votre numéro de groupe).

Voir détails des instructions [ici](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/final-projects/final-projects/)


* [group-01](./group-01.pdf)
* [group-02](./group-02.pdf)
* [group-03](./group-03.pdf)
* [group-04](./group-04.pdf)
* [group-05](./group-05.pdf)
* [group-06](./group-06.pdf)
* [group-07](./group-07.pdf)
* [group-08](./group-08.pdf)
* [group-09](./group-09.pdf)
